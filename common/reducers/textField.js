/**
 * @flow
 */

export type InputType = {
  content: string,
  hot: boolean,
}

type Actions = {
  setContent: string,
  setHot: string,
}
export default function ({ setContent, setHot }: Actions) {
  return (state: Object = { content: '', hot: false }, action: Object) => {
    if (action.type === setContent) {
      return { ...state, content: action.content }
    }
    if (action.type === setHot) {
      return { ...state, hot: action.hot }
    }
    return state
  }
}
