/**
 * @flow
 */

import { applyMiddleware } from 'redux'

function thunkState({ dispatch, getState }) {
  return next => action => {
    if (action && typeof action === 'function') {
      return dispatch(action(getState()))
    }
    return next(action)
  }
}

export default applyMiddleware(
  thunkState,
)
