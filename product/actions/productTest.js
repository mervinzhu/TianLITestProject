/**
 * @flow
 */

export default updateProductTest = (productTest: number) => {
  return {
    type: 'system/test',
      payload: ++productTest,
  }
}
