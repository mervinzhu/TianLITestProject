/**
 * @flow
 */

import { combineReducers } from 'redux'
import productTest from './productTestReducer'

const reducers = {
  test: productTest,
}

export default combineReducers(reducers)
