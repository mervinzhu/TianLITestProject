/**
 * @flow
 */

import { replaceReducer } from '../../common/reducers/common'

export default replaceReducer('system/test', 0)
