/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import updateProductTest from '../actions/productTest'

class ProductPage extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to ProductPage {this.props.product}!
        </Text>
        <TouchableOpacity onPress={() => this.props.updateProductTest(this.props.product)}>
          <Text>click++</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

function mapProps(props) {
  return {
    product: props.test,
  }
}

function mapActions(dispatch) {
  return {
    updateProductTest: (productTest: number) => dispatch(updateProductTest(productTest)),
  }
}

export default connect(mapProps, mapActions)(ProductPage)
