/**
 * @flow
 */
import { Platform, AsyncStorage } from 'react-native'
import devTools from 'remote-redux-devtools'
import { createStore, compose } from 'redux'
import { persistStore, autoRehydrate } from 'redux-persist'
import reducer from './reducers'
import middleWare from '../common/middleWare'


export default async function (onComplete: () => void) {
  /* eslint-disable */
  const devEnhancer = devTools({
    name: Platform.OS,
    hostname: 'localhost',
    port: 5678,
  })
  const productionEnhancer = compose(middleWare, autoRehydrate())
  const fn='__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'
  let devMac= window[fn] ? window[fn]():devEnhancer
  //devMac 和enhancer 二选一,如果使用和enhancer,  package.json 里面postinstall 脚本块需要替换成postinstall-devTools
  const enhancer = __DEV__ ? compose(productionEnhancer, devMac /* enhancer */) : productionEnhancer
  /* eslint-enabled */
  const store = createStore(
    reducer,
    undefined,
    enhancer
  )

  const commonInit = () => {
    onComplete(store)
  }

  const onStoreInit = async () => {
    commonInit()
  }

  if (false) {
    const persistor = persistStore(store, {
      storage: AsyncStorage,
    }, onStoreInit)
    global.ErrorUtils.setGlobalHandler((err) => {
      persistor.purge()
      throw err
    })
  } else {
    persistStore(store, {
      whitelist: [
        'test',
        'test',
      ],
      keyPrefix: 'activity',
      storage: AsyncStorage,
    }, onStoreInit)
  }
}
