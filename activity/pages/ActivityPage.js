/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import updateActivityTest from '../actions/activityTest'

class ActivityPage extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to ActivityPage {this.props.activity}!
        </Text>
        <TouchableOpacity onPress={() => this.props.updateActivityTest(this.props.activity)}>
          <Text>click++</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

function mapProps(props) {
  return {
    activity: props.test,
  }
}

function mapActions(dispatch) {
  return {
    updateActivityTest: (activiyTest: number) => dispatch(updateActivityTest(activiyTest)),
  }
}

export default connect(mapProps, mapActions)(ActivityPage)
