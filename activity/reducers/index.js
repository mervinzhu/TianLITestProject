/**
 * @flow
 */

import { combineReducers } from 'redux'
import activityTest, { a } from './activityTestReducer'

const reducers = {
  test: activityTest,
}

export default combineReducers(reducers)
