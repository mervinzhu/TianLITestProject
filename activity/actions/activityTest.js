/**
 * @flow
 */

export default updateActivityTest = (activityTest: number) => ({
  type: 'system/test',
  payload: ++activityTest,
})
