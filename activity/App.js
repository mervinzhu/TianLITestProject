/**
 * @flow
 */
import React, { Component } from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'
import { Provider } from 'react-redux'
import createStore from './store'
import ActivityPage from './pages/ActivityPage'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

class App extends Component {
  constructor() {
    super()
    this.state = {
      store: null,
    }
  }
  state: {
    store?: any,
  }
  componentDidMount() {
    createStore((store) => {
      this.setState({
        store,
      })
    })
  }

  render() {
    if (this.state.store === null) {
      return null
    }
    return (
      <Provider store={this.state.store}>
        <View style={styles.container}>
          <ActivityPage />
        </View>
      </Provider>
    )
  }
}

export default App
