/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import "RootViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  RootViewController *rootViewCon = [[RootViewController alloc]initWithNibName:@"RootViewController" bundle:nil];
  rootViewCon.launchOptions = launchOptions;
  UINavigationController *navCon = [[UINavigationController alloc]initWithRootViewController:rootViewCon];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  self.window.rootViewController = navCon;
  [self.window makeKeyAndVisible];
  return YES;
}

@end
