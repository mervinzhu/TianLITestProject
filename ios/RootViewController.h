//
//  RootViewController.h
//  TianLiTestProject
//
//  Created by mervin on 2017/2/20.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController

@property (nonatomic, retain) NSDictionary *launchOptions;

@end
